-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 13, 2018 at 07:22 PM
-- Server version: 10.1.37-MariaDB-0+deb9u1
-- PHP Version: 7.2.13-1+0~20181207100540.13+stretch~1.gbpf57305

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rp_recursos`
--

-- --------------------------------------------------------

--
-- Table structure for table `comissao`
--

CREATE TABLE `comissao` (
  `idComissao` int(11) NOT NULL,
  `nomeComissao` varchar(45) DEFAULT NULL,
  `descricaoComissao` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comissao`
--

INSERT INTO `comissao` (`idComissao`, `nomeComissao`, `descricaoComissao`) VALUES
(1, 'Engenharia de Software', 'ES'),
(2, 'Ciência da Computação', 'CC'),
(3, 'Engenharia Elétrica', 'EL');

-- --------------------------------------------------------

--
-- Table structure for table `itempauta`
--

CREATE TABLE `itempauta` (
  `idItemPauta` int(11) NOT NULL,
  `nomeItemPauta` varchar(45) DEFAULT NULL,
  `descricaoItemPauta` varchar(45) DEFAULT NULL,
  `relator` varchar(45) DEFAULT NULL,
  `segundoTurno` varchar(45) DEFAULT NULL,
  `Reuniao_idreuniao` int(11) NOT NULL,
  `fechado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `itempauta`
--

INSERT INTO `itempauta` (`idItemPauta`, `nomeItemPauta`, `descricaoItemPauta`, `relator`, `segundoTurno`, `Reuniao_idreuniao`, `fechado`) VALUES
(1, 'Vamos votar um item legal', 'Esse item pauta é legal', 'Um cara Legal', NULL, 1, 1),
(2, 'Outro item pauta - Esse sim é legal', 'Item Legal', 'Um cara muito legal', NULL, 1, 0),
(3, 'Ok OK ', 'ok ok ok', 'Meu relator', 'null', 2, 0),
(4, 'oi oi', 'hahahah', 'Meu relator', 'null', 2, 0),
(5, 'Disciplinas extras', 'Reunião para discussão das disciplinas', 'Moderador 2', NULL, 4, 1),
(6, 'Discussão Requisitos CC', 'Reunião para discussão de requisitos no curso', 'Moderador 2', NULL, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `opcoesvoto`
--

CREATE TABLE `opcoesvoto` (
  `idOpcoesVoto` int(11) NOT NULL,
  `nomeOpcoesVoto` varchar(45) DEFAULT NULL,
  `ItemPauta_idItemPauta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opcoesvoto`
--

INSERT INTO `opcoesvoto` (`idOpcoesVoto`, `nomeOpcoesVoto`, `ItemPauta_idItemPauta`) VALUES
(321, 'Abstenção', 4),
(322, 'Favoravél', 4),
(323, 'Contrario', 4),
(331, 'Abstenção', 3),
(332, 'Favoravél', 3),
(333, 'Contrario', 3),
(340, 'Abstenção', 4),
(341, 'Abstenção', 4),
(342, '123', 4),
(343, '456', 4),
(419, 'Abstenção', 2),
(420, 'Favoravél', 2),
(421, 'Contrario', 2),
(428, 'Abstenção', 5),
(429, 'Favoravél', 5),
(430, 'Contrario', 5),
(437, 'Abstenção', 6),
(438, 'Favoravél', 6),
(439, 'Contrario', 6),
(440, 'Abstenção', 1),
(441, 'Favoravél', 1),
(442, 'Contrario', 1);

-- --------------------------------------------------------

--
-- Table structure for table `registro`
--

CREATE TABLE `registro` (
  `Reuniao_idreuniao` int(11) NOT NULL,
  `Usuario_idUsuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registro`
--

INSERT INTO `registro` (`Reuniao_idreuniao`, `Usuario_idUsuario`) VALUES
(1, 1),
(4, 2),
(4, 5),
(4, 6);

-- --------------------------------------------------------

--
-- Table structure for table `reuniao`
--

CREATE TABLE `reuniao` (
  `idreuniao` int(11) NOT NULL,
  `nomeReuniao` varchar(45) DEFAULT NULL,
  `descricaoReuniao` varchar(45) DEFAULT NULL,
  `dataReuniao` date DEFAULT NULL,
  `statusReuniao` tinyint(1) DEFAULT NULL,
  `TipoReuniao_idTipoReuniao` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reuniao`
--

INSERT INTO `reuniao` (`idreuniao`, `nomeReuniao`, `descricaoReuniao`, `dataReuniao`, `statusReuniao`, `TipoReuniao_idTipoReuniao`) VALUES
(1, 'Reunião CC', 'Troca o JP', '2018-10-24', -1, 2),
(2, 'Reunião da Engenharia', 'Reunão para adicionar algo legal', '2018-11-30', 0, 2),
(3, 'Nome ok', 'ok OK', '2018-12-17', 0, 1),
(4, 'Nome legal', 'Legal legal', '2018-12-27', -1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tiporeuniao`
--

CREATE TABLE `tiporeuniao` (
  `idTipoReuniao` int(11) NOT NULL,
  `nomeTipoReuniao` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tiporeuniao`
--

INSERT INTO `tiporeuniao` (`idTipoReuniao`, `nomeTipoReuniao`) VALUES
(1, 'Ordinaria'),
(2, 'Extraordinaria');

-- --------------------------------------------------------

--
-- Table structure for table `tipousuariocomissao`
--

CREATE TABLE `tipousuariocomissao` (
  `Tipo_Usuario_reuniao_idreuniao` int(11) NOT NULL,
  `Tipo_Usuario_Usuario_idUsuario` int(11) NOT NULL,
  `Comissao_idComissao` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipousuariocomissao`
--

INSERT INTO `tipousuariocomissao` (`Tipo_Usuario_reuniao_idreuniao`, `Tipo_Usuario_Usuario_idUsuario`, `Comissao_idComissao`) VALUES
(1, 2, 2),
(1, 3, 2),
(2, 2, 2),
(3, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tipo_usuario`
--

CREATE TABLE `tipo_usuario` (
  `reuniao_idreuniao` int(11) NOT NULL,
  `Usuario_idUsuario` int(11) NOT NULL,
  `isModerador` tinyint(4) DEFAULT NULL,
  `isMembro` tinyint(4) DEFAULT NULL,
  `isSecretario` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipo_usuario`
--

INSERT INTO `tipo_usuario` (`reuniao_idreuniao`, `Usuario_idUsuario`, `isModerador`, `isMembro`, `isSecretario`) VALUES
(1, 2, 1, 0, 0),
(1, 3, 0, 0, 1),
(2, 2, 1, 0, 0),
(3, 2, 1, 0, 0),
(4, 4, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `idUsuario` int(11) NOT NULL,
  `nomeUsuario` varchar(100) DEFAULT NULL,
  `siape` varchar(100) DEFAULT NULL,
  `login` varchar(100) DEFAULT NULL,
  `senha` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`idUsuario`, `nomeUsuario`, `siape`, `login`, `senha`) VALUES
(1, 'Membro', '1', 'membro', '12345'),
(2, 'Moderador', '2', 'moderador', '12345'),
(3, 'Secretario', '3', 'secretario', '12345'),
(4, 'Moderador 2', '4', 'moderador2', '12345'),
(5, 'Membro 2', '5', 'membro2', '12345'),
(6, 'Membro 3', '6', 'membro3', '12345');

-- --------------------------------------------------------

--
-- Table structure for table `voto`
--

CREATE TABLE `voto` (
  `Usuario_idUsuario` int(11) NOT NULL,
  `OpcoesVoto_idOpcoesVoto` int(11) NOT NULL,
  `segundoTurno` tinyint(4) DEFAULT NULL,
  `dataVoto` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `voto`
--

INSERT INTO `voto` (`Usuario_idUsuario`, `OpcoesVoto_idOpcoesVoto`, `segundoTurno`, `dataVoto`) VALUES
(1, 441, NULL, NULL),
(2, 429, NULL, NULL),
(5, 428, NULL, NULL),
(5, 438, NULL, NULL),
(6, 438, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comissao`
--
ALTER TABLE `comissao`
  ADD PRIMARY KEY (`idComissao`);

--
-- Indexes for table `itempauta`
--
ALTER TABLE `itempauta`
  ADD PRIMARY KEY (`idItemPauta`),
  ADD KEY `fk_ItemPauta_Reuniao1_idx` (`Reuniao_idreuniao`);

--
-- Indexes for table `opcoesvoto`
--
ALTER TABLE `opcoesvoto`
  ADD PRIMARY KEY (`idOpcoesVoto`),
  ADD KEY `fk_OpcoesVoto_ItemPauta1_idx` (`ItemPauta_idItemPauta`);

--
-- Indexes for table `registro`
--
ALTER TABLE `registro`
  ADD PRIMARY KEY (`Reuniao_idreuniao`,`Usuario_idUsuario`),
  ADD KEY `fk_Registro_Reuniao1_idx` (`Reuniao_idreuniao`),
  ADD KEY `fk_Registro_Usuario1_idx` (`Usuario_idUsuario`);

--
-- Indexes for table `reuniao`
--
ALTER TABLE `reuniao`
  ADD PRIMARY KEY (`idreuniao`),
  ADD KEY `fk_reuniao_TipoReuniao_idx` (`TipoReuniao_idTipoReuniao`);

--
-- Indexes for table `tiporeuniao`
--
ALTER TABLE `tiporeuniao`
  ADD PRIMARY KEY (`idTipoReuniao`);

--
-- Indexes for table `tipousuariocomissao`
--
ALTER TABLE `tipousuariocomissao`
  ADD PRIMARY KEY (`Tipo_Usuario_reuniao_idreuniao`,`Tipo_Usuario_Usuario_idUsuario`,`Comissao_idComissao`),
  ADD KEY `fk_Tipo_Usuario_has_Comissao_Comissao1_idx` (`Comissao_idComissao`),
  ADD KEY `fk_Tipo_Usuario_has_Comissao_Tipo_Usuario1_idx` (`Tipo_Usuario_reuniao_idreuniao`,`Tipo_Usuario_Usuario_idUsuario`);

--
-- Indexes for table `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  ADD PRIMARY KEY (`reuniao_idreuniao`,`Usuario_idUsuario`),
  ADD KEY `fk_reuniao_has_Usuario_Usuario1_idx` (`Usuario_idUsuario`),
  ADD KEY `fk_reuniao_has_Usuario_reuniao1_idx` (`reuniao_idreuniao`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idUsuario`);

--
-- Indexes for table `voto`
--
ALTER TABLE `voto`
  ADD PRIMARY KEY (`Usuario_idUsuario`,`OpcoesVoto_idOpcoesVoto`),
  ADD KEY `fk_Usuario_has_OpcoesVoto_OpcoesVoto1_idx` (`OpcoesVoto_idOpcoesVoto`),
  ADD KEY `fk_Usuario_has_OpcoesVoto_Usuario1_idx` (`Usuario_idUsuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comissao`
--
ALTER TABLE `comissao`
  MODIFY `idComissao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `itempauta`
--
ALTER TABLE `itempauta`
  MODIFY `idItemPauta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `opcoesvoto`
--
ALTER TABLE `opcoesvoto`
  MODIFY `idOpcoesVoto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=449;
--
-- AUTO_INCREMENT for table `reuniao`
--
ALTER TABLE `reuniao`
  MODIFY `idreuniao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `itempauta`
--
ALTER TABLE `itempauta`
  ADD CONSTRAINT `fk_ItemPauta_Reuniao1` FOREIGN KEY (`Reuniao_idreuniao`) REFERENCES `reuniao` (`idreuniao`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `opcoesvoto`
--
ALTER TABLE `opcoesvoto`
  ADD CONSTRAINT `fk_OpcoesVoto_ItemPauta1` FOREIGN KEY (`ItemPauta_idItemPauta`) REFERENCES `itempauta` (`idItemPauta`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `registro`
--
ALTER TABLE `registro`
  ADD CONSTRAINT `fk_Registro_Reuniao1` FOREIGN KEY (`Reuniao_idreuniao`) REFERENCES `reuniao` (`idreuniao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Registro_Usuario1` FOREIGN KEY (`Usuario_idUsuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `reuniao`
--
ALTER TABLE `reuniao`
  ADD CONSTRAINT `fk_reuniao_TipoReuniao` FOREIGN KEY (`TipoReuniao_idTipoReuniao`) REFERENCES `tiporeuniao` (`idTipoReuniao`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tipousuariocomissao`
--
ALTER TABLE `tipousuariocomissao`
  ADD CONSTRAINT `fk_Tipo_Usuario_has_Comissao_Comissao1` FOREIGN KEY (`Comissao_idComissao`) REFERENCES `comissao` (`idComissao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Tipo_Usuario_has_Comissao_Tipo_Usuario1` FOREIGN KEY (`Tipo_Usuario_reuniao_idreuniao`,`Tipo_Usuario_Usuario_idUsuario`) REFERENCES `tipo_usuario` (`reuniao_idreuniao`, `Usuario_idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  ADD CONSTRAINT `fk_reuniao_has_Usuario_Usuario1` FOREIGN KEY (`Usuario_idUsuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_reuniao_has_Usuario_reuniao1` FOREIGN KEY (`reuniao_idreuniao`) REFERENCES `reuniao` (`idreuniao`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `voto`
--
ALTER TABLE `voto`
  ADD CONSTRAINT `fk_Usuario_has_OpcoesVoto_OpcoesVoto1` FOREIGN KEY (`OpcoesVoto_idOpcoesVoto`) REFERENCES `opcoesvoto` (`idOpcoesVoto`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_Usuario_has_OpcoesVoto_Usuario1` FOREIGN KEY (`Usuario_idUsuario`) REFERENCES `usuario` (`idUsuario`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
