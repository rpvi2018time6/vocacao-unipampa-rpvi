<?php
/**
 * Created by PhpStorm.
 * User: Arcano
 * Date: 25/10/2018
 * Time: 02:53
 */

class Encaminhamento extends CI_Model
{

    private $id;
    private $nome;

    public function __construct()
    {
        parent::__construct();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
    }
}