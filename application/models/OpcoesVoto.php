<?php
/**
 * Created by PhpStorm.
 * User: Arcano
 * Date: 25/10/2018
 * Time: 02:53
 */

class OpcoesVoto extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function inserirSimples($id)
    {
        $this->limpaOpcoesVoto($id);
        $sql = "INSERT INTO `opcoesvoto` (`NomeOpcoesVoto`, `ItemPauta_idItemPauta`) VALUES (?, ?), (?, ?), (?, ?)";
        $this->db->query($sql, array('Abstenção', $id, 'Favoravél',$id,  'Contrario' ,$id));

        $this->fechaIP($id);
    }
    public function inserirPersonalizado($id, $opcoes_Voto)
    {
        $sql = "INSERT INTO `opcoesvoto` (`NomeOpcoesVoto`,`ItemPauta_idItemPauta`) VALUES (?,?)";
        foreach ($opcoes_Voto as $opcao){
            $voto=array();
            array_push($voto, $opcao, $id);

            //salvando os votos -> 1 por 1
            $this->db->query($sql, $voto);
        }
        $this->fechaIP($id);
    }

    /**
     * Função que tem como objetivo limpar as opções de voto
     * previamente selecionadas
     *
     * Objetivo: Sobreescrever opções selecionadas anteriormente
     */
    private function limpaOpcoesVoto($id)
    {
        $sql = "DELETE FROM `opcoesvoto` WHERE `opcoesvoto`.`ItemPauta_idItemPauta` = ?";
        return $this->db->query($sql, $id);
    }


    private function fechaIP($id_ip)
    {
        $sql = "UPDATE `itempauta` SET `fechado` = '1' WHERE `itempauta`.`idItemPauta` = ?";
        return $this->db->query($sql, $id_ip);
    }

    public function getReuniaoId($id_ip)
    {
        echo $id_ip;
        $sql = "SELECT Reuniao_idreuniao FROM `itempauta` WHERE `idItemPauta` = ?";
        return $this->db->query($sql, $id_ip);
    }

}