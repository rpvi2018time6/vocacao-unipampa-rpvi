<?php
/**
 * Created by PhpStorm.
 * User: vitao
 * Date: 29/11/2018
 * Time: 18:57
 */

class Votacao_Model extends CI_Model{

    public function buscarVotos($id_item_pauta){
        $sql = "select ip.idItemPauta, ip.nomeItemPauta, count(ov.idOpcoesVoto) as Votos from voto v INNER JOIN opcoesvoto ov ON ov.idOpcoesVoto=v.OpcoesVoto_idOpcoesVoto INNER JOIN itempauta ip ON ip.idItemPauta=ov.ItemPauta_idItemPauta WHERE ip.idItemPauta=?
GROUP by ip.idItemPauta ORDER BY ip.idItemPauta, ip.nomeItemPauta";

        return $this->db->query($sql, $id_item_pauta)->result();

    }
    public function salvaVoto($idUsuario, $idVoto)
    {
        $sql = "INSERT INTO `voto` (`Usuario_idUsuario`, `OpcoesVoto_idOpcoesVoto`, `segundoTurno`, `dataVoto`) VALUES (?, ?, NULL, NULL)";
        return $this->db->query($sql, array($idUsuario, $idVoto));
    }
}