<?php
/**
 * Created by PhpStorm.
 * User: Arcano
 * Date: 21/10/2018
 * Time: 15:43
 */

class Reuniao extends CI_Model
{


    private $id;
    private $nome;
    private $data;
    private $descricao;
    private $comissao;
    private $tipoReuniao;
    private $status;


    public function __construct()
    {
    }


    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTipoReuniao()
    {
        return $this->tipoReuniao;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @return mixed
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }


    /**
     * @return mixed
     */
    public function getComissao()
    {
        return $this->comissao;
    }


    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @param mixed $tipoReuniao
     */
    public function setTipoReuniao($tipoReuniao)
    {
        $this->tipoReuniao = $tipoReuniao;
    }

    /**
     * @param mixed $comissao
     */
    public function setComissao($comissao)
    {
        $this->comissao = $comissao;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @param mixed $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

}