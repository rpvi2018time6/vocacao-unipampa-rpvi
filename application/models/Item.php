<?php
/**
 * Created by PhpStorm.
 * User: Arcano
 * Date: 25/10/2018
 * Time: 02:53
 */


include ("Encaminhamento.php");
include ("Pauta.php");
class Item extends CI_Model
{

    public function __construct()
    {

        parent::__construct();
    }



    public function abrir($idReuniao){
        $abrirReuniao = 'SELECT statusReuniao FROM reuniao WHERE reuniao.idReuniao =?';
        $resultado = $this->db->query($abrirReuniao,$idReuniao);

        $statusReuniao= $resultado->row_array();
        if ($statusReuniao['statusReuniao']=='0') {
            $sqlup = 'UPDATE reuniao SET statusReuniao = \'1\' WHERE reuniao.idReuniao = ?';
            $this->db->query($sqlup, $idReuniao);
            return $this->getByReuniao($idReuniao);
        }
        return null;
    }

    public function fecharReuniao($idReuniao){
        $fecharReuniao = 'UPDATE `reuniao` SET `statusReuniao` = \'-1\' WHERE `reuniao`.`idreuniao` = ?';
        return $this->db->query($fecharReuniao,$idReuniao);
    }

    /**
     * REGISTRAR-SE EM UMA REUNIÃO ABERTA
     * @param $id_reuniao
     * @return bool
     */
    public function registrar($id_reuniao){
        $idMembro = $_SESSION['idUsuario'];

        if ($this->testarRegistroReuniao($id_reuniao, $idMembro) == TRUE) {
            $sqlBusca = 'INSERT INTO `registro` (`Reuniao_idreuniao`, `Usuario_idUsuario`) VALUES (?, ?); ';
            $this->db->query($sqlBusca, array($id_reuniao, $idMembro));
            //registrado na reuniao
            echo "AGUARDE RECEBER O LINK PARA EFETUAR A VOTAÇÃO";
            return TRUE;
        }else{
            echo "VOCÊ JÁ ESTÁ CADASTRADO NESTA REUNIÃO";
            return false;
        }
    }

    public function testarRegistroReuniao($id_reuniao, $id_membro)
    {
        $sqlRegistar = 'SELECT * FROM `reuniao` WHERE reuniao.idreuniao=?';
        $resultado = $this->db->query($sqlRegistar, $id_reuniao)->result()[0];
        $aberta = $resultado->statusReuniao;

        $sqlTestaCadastro = 'SELECT * FROM `registro` WHERE Reuniao_idreuniao=? AND Usuario_idUsuario=?';
        $resultado2 = $this->db->query($sqlTestaCadastro, array($id_reuniao, $id_membro));
        $cadastrado = $resultado2->num_rows();

        //RETORNA TRUE SE A REUNIÃO ESTÁ ABERTA, E
        // SE O USUÁRIO JÁ NÃO ESTÁ CADASTRADO NESTA REUNIÃO
        return ($aberta=='1' && $cadastrado == 0);
    }

    public function get($id)
    {
        $sql = "SELECT * from itempauta ip INNER JOIN opcoesvoto ov WHERE ip.idItemPauta = ov.ItemPauta_idItemPauta and ip.idItemPauta = ?;";
        $result = $this->db->query($sql,$id)->result();
        //var_dump($result);

        //criando objeto pauta
        $itemPauta = new Pauta();
        $itemPauta->setId($result[0]->idItemPauta);
        $itemPauta->setNome($result[0]->nomeItemPauta);
        $itemPauta->setDescricao($result[0]->descricaoItemPauta);
        $itemPauta->setRelator($result[0]->relator);
        $itemPauta->setFechado($result[0]->fechado);

        $encaminhamentos = array();
        foreach ($result as $resultado_obj)
        {
            $encaminhamento = new Encaminhamento();
            $encaminhamento->setId($resultado_obj->idOpcoesVoto);
            $encaminhamento->setNome($resultado_obj->nomeOpcoesVoto);

            //adiciona encaminhamento ao array de encaminhamentos
            array_push($encaminhamentos, $encaminhamento);
        }
        //adiciona o array de encaminhamentos ao itemPauta
        $itemPauta->setEncaminhamentos($encaminhamentos);

        return $itemPauta;
    }

    public function getByReuniao($idReuniao)
    {
        $listPauta = array();

        $sql = 'SELECT * FROM itempauta WHERE reuniao_idReuniao = ?';


        $resultadoItem = $this->db->query($sql, $idReuniao);

        foreach ($resultadoItem->result() as $r) {

            $pauta = new Pauta();
            $pauta->setId($r->idItemPauta);
            $pauta->setNome($r->nomeItemPauta);
            $pauta->setDescricao($r->descricaoItemPauta);
            $pauta->setRelator($r->relator);
            $pauta->setFechado($r->fechado);

            array_push($listPauta, $pauta);
        }

        return array("list" => $listPauta);
    }

    /**
     * Limpa todos os itens de pauta de uma reunião
     * utilidade: para quando uma reunião for reaberta diretamente pelo banco,
     * evitando mau funcionamento do sistema
     * @param $id_ip
     * @return mixed
     */
    public function abreIP($reu_id)
    {
        $sql = "UPDATE `itempauta` SET `fechado` = '0' WHERE `itempauta`.`Reuniao_idreuniao` = ?";
        return $this->db->query($sql, $reu_id);
    }

}