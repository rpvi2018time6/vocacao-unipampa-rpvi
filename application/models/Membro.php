<?php

class Membro extends CI_Model
{

    private $id;
    private $nome;
    private $comisao;
    private $tipoMembro;


    public function __construct()
    {
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @return mixed
     */
    public function getComisao()
    {
        return $this->comisao;
    }

    /**
     * @return mixed
     */
    public function getTipoMembro()
    {
        return $this->tipoMembro;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @param mixed $comisao
     */
    public function setComisao($comisao)
    {
        $this->comisao = $comisao;
    }

    /**
     * @param mixed $tipoMembro
     */
    public function setTipoMembro($tipoMembro)
    {
        $this->tipoMembro = $tipoMembro;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

}

