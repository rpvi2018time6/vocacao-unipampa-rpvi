<?php
/**
 * Created by PhpStorm.
 * User: Arcano
 * Date: 24/10/2018
 * Time: 20:05
 */

class Pauta extends CI_Model{


    private $id;
    private $nome;
    private $descricao;
    private $relator;
    private $encaminhamentos;
    private $fechado;

    public function  __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getRelator()
    {
        return $this->relator;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }


    /**
     * @param mixed $relator
     */
    public function setRelator($relator)
    {
        $this->relator = $relator;
    }
    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    public function setEncaminhamentos($encaminhamentos)
    {
        $this->encaminhamentos = $encaminhamentos;
    }

    public function getEncaminhamentos()
    {
        return $this->encaminhamentos;
    }

    public function setFechado($fechado)
    {
        $this->fechado = $fechado;
    }

    public function getFechado()
    {
        return $this->fechado;
    }


}