<?php
/**
 * Created by PhpStorm.
 * User: Arcano
 * Date: 21/10/2018
 * Time: 15:47
 */


include("Membro.php");
include("Reuniao.php");

class Login_Model extends CI_Model
{


    /* public function logar($login, $senha){

                 $sql = 'SELECT idUsuario, nomeUsuario FROM usuario where usuario.login = ? AND usuario.senha =? ';


                 $query = $this->db->query($sql, array($login, $senha));


                 if ($query->num_rows() == 0) {

                     $erro = 'Login ou senha incorreto!';
                     $this->load->view('templates/header');
                     $this->load->view('pages/index.php', $erro);
                     $this->load->view('templates/footer');


                     return null;

                 }


                 foreach ($query->result() as $d) {

                     $membro = new Membro();
                     $membro->setNome($d->nomeUsuario);
                     $idMembro = $d->idUsuario;

                 }
                 return array($membro, $idMembro);

             }
 */


    public function buscarReuniao($idMembro)
    {


        $reuniaoMembro = array();
        $reuniaoModerador = array();
        $reuniaoSecretario = array();

//        $sql = 'SELECT r.idReuniao,r.nomeReuniao,r.descricaoReuniao, r.dataReuniao,r.statusReuniao, tu.isModerador, tu.isMembro,tu.isSecretario
//	FROM reuniao r
//    INNER JOIN tipo_usuario tu
//    	ON
//        r.idreuniao=tu.reuniao_idreuniao
//    INNER JOIN usuario u
//    	ON
//        u.idUsuario=tu.Usuario_idUsuario
//    WHERE u.idUsuario = ?';

    $sql = 'SELECT u.idUsuario, r.idReuniao,r.nomeReuniao,r.descricaoReuniao, r.dataReuniao,r.statusReuniao, tu.isModerador, tu.isMembro,tu.isSecretario 
FROM reuniao r
LEFT JOIN tipo_usuario tu 
	ON
    r.idreuniao=tu.reuniao_idreuniao 
LEFT JOIN usuario u
	ON u.idUsuario=tu.Usuario_idUsuario
WHERE u.idUsuario=?
	OR
    (tu.isModerador=1 AND u.idUsuario != ?);';

        $query = $this->db->query($sql, array($idMembro, $idMembro));


        foreach ($query->result() as $r) {
            $reuniao = New Reuniao();
            $reuniao->setId($r->idReuniao);
            $reuniao->setNome($r->nomeReuniao);
            $reuniao->setDescricao($r->descricaoReuniao);
            $reuniao->setData($r->dataReuniao);
            $reuniao->setStatus($r->statusReuniao);
           // $reuniao->setComissao($r->nomeComissao);


            if ($r->isModerador == 1 && $r->idUsuario == $idMembro){
                array_push($reuniaoModerador, $reuniao);
            }else{
                array_push($reuniaoMembro, $reuniao);
            }

            //NÃO REMOVIDO PRA NÃO CAUSAR ERROS AINDA...
            if ($r->isSecretario == 1){

                array_push($reuniaoSecretario, $reuniao);
            }
            /* switch ($r->idTipoUsuario) {
                 case 1:
                     array_push($reuniaoMembro, $reuniao);
                     break;
                 case 2:
                     array_push($reuniaoModerador, $reuniao);
                     break;
                 case 3:

                     array_push($reuniaoSecretario, $reuniao);
                     break;
             }*/


        }


        return array('membro' => $reuniaoMembro, 'moderador' => $reuniaoModerador, 'secretario' => $reuniaoSecretario);


    }


}