<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <title>Teste</title>
</head>
<body
    <div class="container">
        <h1>Produtos</h1>
        <table class="table">
            <tr>
                <th>Codigo</th>
                <th>Info 1</th>
                <th>Info 2</th>
                <th>Info 3</th>
            </tr>
            <?php foreach($testes as $teste): ?>
            <tr>
                <td><?= $teste['idTabelaTeste'] ?></td>
                <td><?= formatarReais($teste['infoTeste01']) ?></td>
                <td><?= $teste['infoTeste02'] ?></td>
                <td><?= formatarData($teste['infoTeste03']) ?></td>
                <td></td>
                <td></td>
            </tr>
            <?php endforeach ?>
        </table>

        <h1>Cadastro Teste Users</h1>
        <?php
            echo form_open("usuarios/novo");
            echo form_label("Nome", "nome");
            echo form_input(array(
                    "name"=>"nome",
                    "id"=>"nome",
                    "class"=>"form-control",
                    "maxlenght"=>255
            ));

            echo form_label("Email", "email");
            echo form_input(array(
                "name"=>"email",
                "id"=>"email",
                "class"=>"form-control",
                "maxlenght"=>255
            ));

            echo form_label("Senha", "senha");
            echo form_password(array(
                "name"=>"senha",
                "id"=>"senha",
                "class"=>"form-control",
                "maxlenght"=>255
            ));

            echo form_button(array(
                    "class"=>"btn btn-primary",
                    "type"=>"submit",
                    "content"=>"Cadastrar"
            ));
            echo form_close();
        ?>
    </div>
</body>
</html>
