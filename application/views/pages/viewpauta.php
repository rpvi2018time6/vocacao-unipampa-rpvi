<div class="container">
    <h2 style="text-align: center; color: #009045">Bem Vindo ao Painel de Itens de Pauta</h2>


    <?php if ($itempauta!= null){ ?>
    <form method="POST">
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>Nome</th>
                <th>Descrição</th>
                <th>Relator</th>
                <th>Ação</th>
            </tr>
            </thead>
            <tbody>




            <?php foreach ($itempauta as $r) { ?>
            <tr>

                <th> <?= $r->getNome() ?></th>
                <th> <?= $r->getDescricao() ?></th>
                <th> <?= $r->getRelator() ?></th>
                <th>


                    <?php if ($r->getFechado() == 0): ?>
                        <a  class="btn btn-success" href="<?= base_url("CriarVoto/index/" . $r->getId()) ?>">Encaminhamentos</a>
                    <?php endif; ?>
                    <?php if ($r->getFechado() == 1): ?>
                        <a  class="btn btn-success" disabled href="#"">Votação encerrada...</a>
                    <?php endif; ?>

                </th>


                <?php } ?>
            </tr>
            </tbody>
        </table>
    </form>

        <?php } else { ?>
            <h2>Esta reunião não possui itens de pauta! Contate o secretário para adicionar itens de pauta à esta reunião</h2>
        <?php } ?>
    <div class="row col-sm-12">
        <?= form_open("ItemPauta/fecharReuniao") ?>
        <input type="hidden" name="id_reuniao" value="<?= $id_reuniao ?>" />
        <button class="btn btn-danger btn-lg" style="margin: 0 auto;">Fechar reunião</button>
        <?= form_close() ?>
    </div>

</div>

