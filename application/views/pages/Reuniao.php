<div class="container">
    <h2 style="text-align: center; color: #009045">Bem Vindo ao Painel de Reunião</h2>

    <hr />
    <br />
    <br />

    <h2>Reuniões como Moderador:</h2>
    <?php if ($reuniaoModerador != null){ ?>
    <from>
        <table class="table">
            <thead>
            <tr>
                <th>Nome</th>
                <th>Descrição</th>
                <th>Comissão</th>
                <th>Data</th>
                <th>Acão</th>
            </tr>
            </thead>
            <tbody>

            <?php } ?>


            <?php foreach ($reuniaoModerador as $reuniao) { ?>
            <tr>
                <th> <?= $reuniao->getNome() . $reuniao->getStatus() ?></th>
                <th> <?= $reuniao->getDescricao() ?></th>
                <th> <?= $reuniao->getComissao() ?></th>
                <th> <?= $reuniao->getData() ?></th>
                <th>
                    <?php if($reuniao->getStatus() == 0): ?>
                        <a class="btn btn-success" href="<?= base_url("ItemPauta/abrir/" . $reuniao->getId()) ?>">Abrir Reunião</a>
                    <?php elseif($reuniao->getStatus() == 1): ?>
                        <a class="btn btn-success" href="<?= base_url("ItemPauta/ver/" . $reuniao->getId()) ?>">Reunião em andamento...</a>
                    <?php elseif($reuniao->getStatus() == -1): ?>
                        <a class="btn btn-success" disabled>Reunião Fechada</a>
                    <?php endif; ?>

                </th>


                <?php } ?>
            </tr>
            </tbody>
        </table>

        <hr />
        <br />
        <br />
        <h2>Reuniões disponíveis para participação:</h2>

        <?php if ($reuniaoMembro != null){ ?>
        <form>
            <table class="table">
                <thead>
                <tr>
                    <th>Nome</th>
                    <th>Descrição</th>
                    <th>Comissão</th>
                    <th>Data</th>
                    <th>Acão</th>
                </tr>
                </thead>
                <tbody>

                <?php } ?>

                <?php foreach ($reuniaoMembro as $reuniao) { ?>
                    <tr>

                        <th> <?= $reuniao->getNome() ?></th>
                        <th> <?= $reuniao->getDescricao() ?></th>
                        <th> <?= $reuniao->getComissao() ?></th>
                        <th> <?= $reuniao->getData() ?></th>
                        <th>

                            <?php if($reuniao->getStatus() == 1): ?>
                                <a class="btn btn-success"
                                    href="<?= base_url("ItemPauta/registrar/" . $reuniao->getId()) ?>">Cadastrar-se
                                </a>
                            <?php else: ?>
                                <a class="btn btn-success"
                                   href="#" disabled>Reunião fechada...
                                </a>
                            <?php endif;?>
                        </th>


                    </tr>


                <?php } ?>
                </tr>
                </tbody>
            </table>

        </form>

<!--
        <?php if ($reuniaoSecretario != null){ ?>

        <form>

            <table class="table">
                <thead>
                <tr>
                    <th>Nome</th>
                    <th>Descrição</th>
                    <th>Comissão</th>
                    <th>Data</th>
                    <th>Acão</th>
                </tr>
                </thead>
                <tbody>

                <?php } ?>
                <?php foreach ($reuniaoSecretario as $reuniao) { ?>
                    <tr>

                        <th> <?= $reuniao->getNome()?></th>
                        <th> <?= $reuniao->getDescricao() ?></th>
                        <th> <?= $reuniao->getComissao() ?></th>
                        <th> <?= $reuniao->getData() ?></th>
                        <th>

                            <a class="btn btn-success"
                               href="<?php base_url()?>../ItemPauta/abrir/<?= $reuniao->getId(); ?>">
                                GERENCIAR
                            </a>
                        </th>


                    </tr>


                <?php } ?>

                </tr>
                </tbody>
            </table>

        </form>
-->

</div>

