
<div class="text-center mb-4">
<div class="novo_caixa-login" style="max-width: 100%!important;">

    <p class="novo_topo-login">
        <span class="novo_titulo-login">Faça seu Login</span>

        <img src="assets/imagens/instrucoes.png" style="vertical-align:inherit; text-align: right"
             alt="Instruções"/>
    </p>


    <?php echo form_open('Login/set_session', array('class' => 'form-signin')); ?>

    <?php if( isset($erro)): ?>
        <?= "<div class=\"alert alert-danger\" role=\"alert\"><p>$erro</p></div>" ?>
    <?php endif; ?>


    <label for="inputEmail">Usuário</label>
    <input type="text" class="novo_input-login form-control novo_back-user" name='login' id='login'
           placeholder="Name User" required autofocus/>

    </br>



        <label for="inputPassword">Senha</label>
        <input type="password" id="senha" name="senha" class="novo_input-login novo_back-senha form-control" placeholder="Senha" required>


        </br>


        <input type="hidden" name="link" value="<?= $link ?>" />
        <button class="btn btn-lg btn-success btn-block novo_logar" id="entrar" type="submit">Login</button>

        <p class="mt-5 mb-3 text-muted">&copy; 2018</p>
    </form>


</div>
</div>