    <div class="container" style="max-width: 800px">
        <h2 style="text-align: center; color: #009045">Votação do item: <?= $itemPauta->getNome() ?></h2>
        <div class="col-sm-12 text-center">
            <h3>Descrição: <?= $itemPauta->getDescricao() ?></h3>
            <h3>Relator: <?= $itemPauta->getRelator() ?></h3>
        </div>
        <hr />
        <?= form_open("Votar/salva_voto") ?>
            <div class="form-group">
                <label for="sel1" STYLE="color: #009045">ESCOLHA UMA OPÇÃO:</label>
                <select class="form-control" id="sel1" style="height:50px;margin: 0 auto;" name="voto">
                    <?php foreach($itemPauta->getEncaminhamentos() as $encaminhamento): ?>
                        <?= '<option value="'. $encaminhamento->getId() .'">' . $encaminhamento->getNome() . '</option>' ?>
                    <?php endforeach; ?>
                </select>
                <br>
            </div>
            <button class="btn btn-lg btn-block btn-success">Submeter voto</button>
        <?= form_close() ?>
    </div>
