<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?= base_url('assets/imagens/favicon-guri16.png') ?>" type="image/png"/>
    <link rel="stylesheet" href="<?= base_url('assets/css/estilo_menu.css') ?>" type="text/css"/>
    <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>" type="text/css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!----JQUERY----->
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="jquery.dataTables.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script language="JavaScript" src="https://cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script language="JavaScript" src="https://cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>


</head>
<body>


<div class="reset">
    <div id="novo_tarja-topo"></div>
    <div id="novo_topo">

        <h1 id="novo_guri-marca">
            <a href="<?= base_url()?>" title="GURI">
                <img style="max-width: 100%!important;" id="novo_marca"
                     src="https://guri.unipampa.edu.br/public/themes/moder/imgs/logo_guri.png"
                     alt="GURI"/>
            </a>
        </h1>

        <h2 id="novo_unipampa">
            <a href="<?= base_url() ?>" title="Acessar Portal da UNIPAMPA" target="_new">
                <img style="max-width: 100%!important;" class="novo_marca-unipampa"
                     src="https://guri.unipampa.edu.br/public/themes/moder/imgs/marca.png"
                     alt="Acessar Portal da UNIPAMPA"/>
            </a>
        </h2>
        <!--
                <div id="novo_menu-auxiliar">

                    <div class="novo_login">
                        <a class="novo_botao-login" href="https://guri.unipampa.edu.br/ptl/sistema/showLogin">
                            <img  src="https://guri.unipampa.edu.br/public/themes/moder/imgs/login.png" alt="Login"
                                 style="vertical-align:-2px;"/> Login
                        </a>

                    </div>
                </div>-->
    </div>
</div>


<div id="novo_menu" style="max-width: 100%!important;">

    <ul id="novo_menu-principal" style="max-width:100%!important; ">


        <li class="novo_menu">
            <a href="#" class="novo_link-menu" id="area_menu_1">VOTAÇÃO &nbsp;&rsaquo;</a>
            <div id="div_area_menu_1" class="novo_submenu-quadro" style="max-width: 100%!important;">
                <ul class="novo_submenu1">


                    <li><a href="#" class="novo_link-submenu">ORÇAMENTO E FINANÇAS</a></li>


                    <li><a href="#" class="novo_link-submenu">RECURSOS HUMANOS</a></li>


                    <li><a href="#" class="novo_link-submenu">PROCESSOS</a></li>


                    <li><a href="#" class="novo_link-submenu">SERVIÇOS</a></li>


                    <li><a href="#" class="novo_link-submenu">RESTAURANTE</a></li>
                </ul>


            </div>


        <li class="novo_menu" style="max-width: 100%!important;">
            <a href="#" class="novo_link-menu" id="area_menu_2">MEMBRO &nbsp;&rsaquo;</a>
            <div id="div_area_menu_2" class="novo_submenu-quadro">
                <ul class="novo_submenu2">


                    <li><a href="#" class="novo_link-submenu">PROCESSO SELETIVO</a></li>


                    <li><a href="#" class="novo_link-submenu">EVENTOS</a></li>


                    <li><a href="#" class="novo_link-submenu">BIBLIOTECA</a></li>
                </ul>
            </div>

        </li>


        <li class="novo_menu" style="max-width: 100%!important;">
            <a href="#" class="novo_link-menu" id="area_menu_3">MEMBRO &nbsp;&rsaquo;</a>
            <div id="div_area_menu_3" class="novo_submenu-quadro">
                <ul class="novo_submenu2">


                    <li><a href="#" class="novo_link-submenu">PROCESSO SELETIVO</a></li>


                    <li><a href="#" class="novo_link-submenu">EVENTOS</a></li>


                    <li><a href="#" class="novo_link-submenu">BIBLIOTECA</a></li>
                </ul>
            </div>

        </li>
        <li class="novo_menu" style="max-width: 100% !important;">
            <a href="<?= base_url("Login/logout") ?>" class="novo_link-menu" id="area_menu_3">LOGOUT</a>
        </li>
    </ul>

    <div id="novo_busca" style="max-width: 100%!important;">
        <p class="novo_fundo-busca">
            <input type="text" name="txtPesquisaArvore" id="txtIdPesquisaArvore"
                   class="novo_text-busca"
                   value="Pesquisar..."
                   size="40" maxlength="80" title="Informe o nome do módulo ou item de menu a ser acessado"
                   onblur="this.value = 'Pesquisar...';"
                   onfocus="if (this.value == 'Pesquisar...') this.value = ''; limparArvorePesquisa()"/>


<!--            <img id="loadingItens" class="loading" alt="Carregando" style="position:fixed; top: 15px; left: -13px;"-->
<!--                 src='.../assets/imagens/ajax-loader-branco.gif'/>-->


            <input type="submit" value="" class="novo_botao-busca"/>
        </p>
    </div>

</div>


