<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//ini_set('session.cache_limiter', 'public');
//session_cache_limiter(FALSE);

function testa_login() {
    if(!isset($_SESSION['logged_in'])){

        //CAPTURANDO PARAMETROS DA URL
        //QUANDO O USUÁRIO ACESSA O SISTEMA ANTES DE LOGAR,
        //ENTÃO É REDIRECIONADO PARA O LINK QUE TENTOU ACESSAR, APÓS O LOGIN
        $current_url =& get_instance(); //  get a reference to CodeIgniter
        $classe = $current_url->router->fetch_class();
        $metodo = $current_url->router->fetch_method();
        $parametro = $current_url->uri->segment('3');

        $url = "pages/Index/";
        if ($classe !== null) {
           $url .= $classe . "-";
           if ($metodo !== null) {
               $url .= $metodo . "-";
            if ($parametro !== null) {
               $url .= $parametro . "-";
            }
           }
        }

       redirect($url);
    }
}

//function sec_cria2($user) {
//    $_SESSION = array(
//        'sec_id' => (int) $user[0],
//        'sec_email' => (string) $user[1],
//        'sec_texto' => (string) $user[2]
//    );
//    return TRUE;
//}
//
//function sec_login2() {
//    if (isset($_SESSION["sec_email"])) {
//        return TRUE;
//    }
//    return FALSE;
//}
