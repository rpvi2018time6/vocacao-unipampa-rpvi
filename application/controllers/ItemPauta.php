<?php
/**
 * Created by PhpStorm.
 * User: Arcano
 * Date: 25/10/2018
 * Time: 02:51
 */


class ItemPauta extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->Model("item");
        testa_login();
    }


    /**
     * Recebe o id da reunião
     * @param $id
     */
    public function abrir($id)
    {
        //abre a reunião
        $this->item->abrir($id);

        //abre todos os itens de pauta
        $this->item->abreIP($id);

        $this->ver($id);
    }

    /**
     * RECEBE O ID DA REUNIAO
     * @param $id
     */
    public function ver($id)
    {
        $resultado = $this->item->getByReuniao($id);
        $dado['itempauta'] = $resultado['list'];
        $dado['id_reuniao'] = $id;

        $this->load->view('templates/header');
        $this->load->view('pages/viewpauta.php', $dado);
        $this->load->view('templates/footer');
    }

    public function registrar($id)
    {
        $this->load->Model("item");

        $resultado = $this->item->registrar($id);
        if ($resultado == true) {
            //$this->load->view('templates/header');
            //$this->load->view('pages/Voto.php');
            //$this->load->view('templates/footer');
        }
    }

    public function fecharReuniao()
    {
        $idReuniao = $this->input->post("id_reuniao");
        $this->item->fecharReuniao($idReuniao);
        redirect();
    }
}