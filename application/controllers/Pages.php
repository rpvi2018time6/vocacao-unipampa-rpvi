<?php
/**
 * Created by PhpStorm.
 * User: vitao
 * Date: 18/10/2018
 * Time: 20:57
 */
class Pages extends CI_Controller{

    public function view($page = 'Index', $link = NULL){
        if(!file_exists(APPPATH.'views/pages/'.$page.'.php')){
            show_404();
        }

        $data['link'] = "";
        if($link !== NULL)
        {
            //transforma a string recebida em link
            $link = str_replace("-", "/", $link);

            $data['link'] = $link;
        }

        $data['title'] = ucfirst($page);

        $this->load->view('templates/header_login', $data);
        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer', $data);
    }



}