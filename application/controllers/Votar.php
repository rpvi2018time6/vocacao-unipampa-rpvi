<?php

class Votar extends CI_Controller {

    public function __construct() {
        parent:: __construct();
        testa_login();
    }

    public function item_pauta($id){
        $this->load->model('item');
        $data['itemPauta'] = $this->item->get($id);

        $this->load->view('templates/header');
        $this->load->view('pages/Voto.php', $data);
        $this->load->view('templates/footer');
    }

    public function salva_voto(){
        $idUsuario = $_SESSION['idUsuario'];
        $idVoto = $this->input->post("voto");

        $this->load->model('Votacao_Model');
        if ($this->Votacao_Model->salvaVoto($idUsuario, $idVoto) == FALSE){
            $dados['heading'] = "Erro!";
            $dados['message'] = "Seu voto já foi cadastrado!";
            //$this->load->view('html/error_general', $dados);


            echo "n foi";

            return FALSE;
        }

        $this->load->view('templates/header');
        $this->load->view('pages/voto_sucesso');
        $this->load->view('templates/footer');
        return TRUE;
    }
}