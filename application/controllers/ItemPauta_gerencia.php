<?php
/**
 * Created by PhpStorm.
 * User: Arcano
 * Date: 25/10/2018
 * Time: 02:51
 */



class ItemPauta_gerencia extends CI_Controller{


    public function __construct()
    {
        parent::__construct();
        testa_login();
    }


    public function  gerenciar($id){



        $this->load->Model("item");

        $resultado = $this->item->abrir($id);



        $dado['itempauta'] = $resultado['list'];

        $this->load->view('templates/header');
        $this->load->view('pages/viewpauta.php', $dado);
        $this->load->view('templates/footer');

    }
}