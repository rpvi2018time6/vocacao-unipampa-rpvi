<?php
defined("BASEPATH") OR exit('No direct script access allowed');
class Teste extends CI_Controller {
    public function index(){
        $this->load->model("teste_model");
        $lista = $this->teste_model->buscaTodos();
        $dados = array("testes" => $lista);
        $this->load->view('teste/index', $dados);

    }
}
