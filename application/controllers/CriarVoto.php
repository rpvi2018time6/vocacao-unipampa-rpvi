<?php
/**
 * Created by PhpStorm.
 * User: vitao
 * Date: 18/10/2018
 * Time: 20:57
 */
class CriarVoto extends CI_Controller{

    var $idVotacao;

    public function __construct()
    {
        parent::__construct();
        testa_login();
        $this->load->Model("OpcoesVoto");
    }

    /*
     * Recebe o id do item de pauta
     */
    public function index($id_ip){
        $data['idVoto'] = $id_ip;
        //var_dump($data);
        $this->load->view('templates/header', $data);
        $this->load->view('pages/criarvoto.php', $data);
        $this->load->view('templates/footer', $data);
    }

    public function inserirBancoSimples($id_ip){
        $this->OpcoesVoto->inserirSimples($id_ip);

        $this->opcoes_salvas($id_ip);
    }

    public function inserirBancoPerso($id_ip){
        $opcoes_Voto = $this->input->post('opcoes');

        $this->OpcoesVoto->inserirPersonalizado($id_ip, $opcoes_Voto);

        $this->opcoes_salvas($id_ip);
    }

   public function opcoes_salvas($id_ip)
   {
       // Pega o id da reunião para redirecionar o usuário para a página da reunião.
       $id_reuniao = $this->OpcoesVoto->getReuniaoId($id_ip)->result()[0]->Reuniao_idreuniao;

       $dados['id_reuniao'] = $id_reuniao;
       $dados['id_ip'] = $id_ip;
       $this->load->view('templates/header');
       $this->load->view('pages/resultadoVotacao.php', $dados);
       $this->load->view('templates/footer');
   }


}