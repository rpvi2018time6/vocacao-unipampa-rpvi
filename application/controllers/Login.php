<?php
/**
 * Created by PhpStorm.
 * User: Arcano
 * Date: 20/10/2018
 * Time: 12:22
 */


class Login extends CI_Controller
{

    public function __construct() {
        parent:: __construct();
    }

    public function set_session()
    {
        $login = $this->input->post("login");
        $senha =$this->input->post("senha");

        //link para redirecionamento
        $link =$this->input->post("link");

        $sql = 'SELECT idUsuario, nomeUsuario FROM usuario where usuario.login = ? AND usuario.senha =? ';
        // $sql = "SELECT  idUsuario From usuario where login = '" . $login . "' LIMIT1";
        $result = $this->db->query($sql, array($login,$senha));
        $row = $result->row();


        if ($result->num_rows() == 0) {

            $data['link'] = $link;

            $data['erro'] = 'Login ou senha incorretos!';
            $this->load->view('templates/header');
            $this->load->view('pages/Index.php', $data);
            $this->load->view('templates/footer');


            return null;

        }

        $sess_data = array(
            'idUsuario' => $row->idUsuario,
            'nomeUsuario' => $row->nomeUsuario,
            'login' => $login,
            'logged_in' => TRUE,
        );

        $this->session->set_userdata($sess_data);
       # $this->buscarReuniaoSession();

        if ($link !== NULL) {
           redirect($link);
        } else {
            redirect("login/buscarReuniaoSession");
        }
    }


    public function buscarReuniaoSession(){

        testa_login();

        var_dump($this->session-> idUsuario);
        $this->load->Model("Login_Model");

        $reuniao = $this->Login_Model->buscarReuniao($this->session->idUsuario);

        $reuniaoList['reuniaoMembro'] = $reuniao['membro'];
        $reuniaoList['reuniaoModerador'] = $reuniao['moderador'];
        $reuniaoList['reuniaoSecretario'] = $reuniao['secretario'];

        $this->load->view('templates/header');
        $this->load->view('pages/Reuniao.php', $reuniaoList);

        $this->load->view('templates/footer');

    }


    public function logout()
    {
        $_SESSION = array();

        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }
        session_destroy();

        redirect();
    }
}
